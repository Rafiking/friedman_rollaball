﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;

    Rigidbody rb;
    private int count;

    bool onGround = true; // if the player is on the ground. If so, they may jump. 
    public float jumpPower; // how high the player will jump

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    private void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);
        // checks to see if the player is on the ground

        if(Input.GetButton("Jump") && onGround)
        {
            Jump(); // if on the ground, jumps
        }

        if (transform.position.y < -2)
            RestartLevel();
        // game restarts if you fall past -2 on the y-axis
    }

    void Jump()
    {
        rb.AddForce(Vector3.up * jumpPower); //adds force to jump
        var number = Random.Range( 0, 2); //generates a random number whenever you jump
        if (number >= 1) 
            transform.position = new Vector3(Random.Range(-9,9), 1, Random.Range(-9, 9)); 
        // if the number is greater than or equal to 1, the player is randomly placed somewhere on the game table everytime you jump.
        print(number);
        
    }
        
    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win";

            Invoke("RestartLevel", 4f);
            // game restarts after 4 seconds
        }
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        // lets you reload the current level
    }
}